//
//  AlbumCollectionViewCell.swift
//  wedding
//
//  Created by Bofu Alex on 20/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit
import AlamofireImage

class AlbumCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var albumImageView: UIImageView!
    @IBOutlet weak var albumNameLabel: UILabel!
    @IBOutlet weak var albumDateLabel: UILabel!
    
    func setupCell(_ album: Album) {
        albumImageView.layer.borderWidth = 5
        albumImageView.layer.borderColor = UIColor.random().cgColor
        albumNameLabel.text = album.albumName
        albumDateLabel.text = album.createdAt
    }
}
