//
//  FStorage.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import Firebase

struct FStorage: Firebase {
    
    static func uploadPhoto(_ photo: Data, to album: String) {
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/png"
        
        storageRef.child(album).put(photo, metadata: metadata, completion: { (metadata, error) in
            
        })
    }
    
    static func uploadUserPhoto(_ photo: Data, user: User) {
        var user = user
        let metadata = FIRStorageMetadata()
        metadata.contentType = "image/png"
        
        storageRef.child(user.uuid).put(photo, metadata: metadata, completion: { (metadata, error) in
            if error == nil {
                if let metadata = metadata?.downloadURL() {
                    user.photo = metadata.absoluteString
                }
            }
            
            FDatabase.createUser(user)
        })
    }
}
