//
//  Album.swift
//  wedding
//
//  Created by Bofu Alex on 21/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

struct Album {
    var albumName: String!
    var createdAt: String!
    var albumCoverLink: String?
    var albumCoverImage: UIImage?
}

extension Album {
    static func parseAlbumCovers(_ dict: Dictionary<AnyHashable, Any>) -> [Album] {
        var albums = [Album]()
        
        for (key, value) in dict {
            var album = Album()
            
            if let name = key as? String {
                album.albumName = name
            }
            
            if let value = value as? [String: String] {
                album.createdAt = value[Constants.Firebase.Database.CREATEDAT]
                album.albumCoverLink = value[Constants.Firebase.Database.COVER]
            }
            
            albums.append(album)
        }
        
        albums.sort(by: { $0.createdAt < $1.createdAt})
        
        return albums
    }
}
