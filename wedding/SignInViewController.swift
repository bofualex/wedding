//
//  SignInViewController.swift
//  wedding
//
//  Created by Bofu Alex on 19/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SignInViewDelegate: class {
    func signInViewRequestsSignIn(email: String, password: String)
}

class SignInViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var innerView: UIScrollView!
    
    weak var delegate: SignInViewDelegate?
    
    fileprivate var activeField: UITextField?

    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotifications()
    }
    
    //MARK: - memory management
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - private
    fileprivate func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: - action
    @IBAction func onBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func onSignIn(_ sender: UIButton) {
        guard emailTextField.text!.characters.count > 3 && emailTextField.text!.contains("@") else {
            AlertViewController.showAlert(on: self, message: "Email address is badly formatted", title: "Email address", buttonText: "OK")
            return
        }
        
        guard passwordTextField.text!.characters.count > 4 else {
            AlertViewController.showAlert(on: self, message: "Please enter a longer password", title: "Longer password required", buttonText: "OK")
            return
        }
        
        SVProgressHUD.show()
        delegate?.signInViewRequestsSignIn(email: emailTextField.text!, password: passwordTextField.text!)
    }
}

extension SignInViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
}

extension SignInViewController {
    func keyboardWillShow(notification: NSNotification) {
        if let activeField = activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            innerView.contentInset = contentInsets
            innerView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                innerView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        innerView.contentInset = contentInsets
        innerView.scrollIndicatorInsets = contentInsets
    }
}
