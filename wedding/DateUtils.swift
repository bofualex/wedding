//
//  DateUtils.swift
//  wedding
//
//  Created by Bofu Alex on 19/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
extension Date {
    static func dateFromString(_ date: String?) -> Date {
        guard let date = date else {
            return Date()
        }
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateToDisplay = formatter.date(from: date)
        return dateToDisplay!
    }
    
    func hours(from date: Date) -> Int {
        return Calendar.current.dateComponents([.hour], from: date, to: self).hour ?? 0
    }
    
    func formatDate(_ date: Date) -> String {
        let formatter = DateFormatter()
        let day_seconds: TimeInterval = -86400
        
        if date.timeIntervalSinceNow < day_seconds {
            formatter.dateStyle = .long
            formatter.timeStyle = .none
            let dateToDisplay = formatter.string(from: date)
            return dateToDisplay
        } else {
            formatter.timeStyle = .short
            let dateToDisplay = formatter.string(from: date)
            return dateToDisplay
        }
    }
    
    func formatWithoutZone(_ date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateToDisplay = formatter.string(from: date)
        return dateToDisplay
    }
}
