//
//  CameraViewController.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit

final class CameraViewController: UIViewController {

    fileprivate var photoData: Data?
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK: - action
    @IBAction func onAddPhoto(_ sender: UIButton) {
        
    }
    
    @IBAction func onCreateAlbum(_ sender: UIButton) {
        view.endEditing(true)
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        let cameraAlert = UIAlertController(title: title, message: "Take a photo of your dish", preferredStyle: UIAlertControllerStyle.alert)
        
        cameraAlert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { [weak self]
            (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        cameraAlert.addAction(UIAlertAction(title: "Choose Photos", style: .default, handler: { [weak self]
            (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        cameraAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        present(cameraAlert, animated: true, completion: nil)
    }
}

extension CameraViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            photoData = UIImageJPEGRepresentation(pickedImage, 0.3)!
        }
    }
}

