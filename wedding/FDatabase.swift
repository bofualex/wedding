//
//  FDatabase.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import Firebase

struct FDatabase: Firebase {
    
    static func createUser(_ user: User) {
        var userItem = [Constants.Firebase.Database.NAME: user.name, Constants.Firebase.Database.EMAIL: user.email, Constants.Firebase.Database.UUID: user.uuid, Constants.Firebase.Database.CREATEDAT: Date().formatWithoutZone(Date()), Constants.Firebase.Database.MODIFIEDAT: Date().formatWithoutZone(Date())]
        
        if user.photo != nil {
            userItem[Constants.Firebase.Database.PHOTO] = user.photo
        }
        
        databaseRef.child("\(Constants.Firebase.Path.USER)/\(user.uuid!)").updateChildValues(userItem, withCompletionBlock: { (snapshot) in
            FAuth.saveLocally(userItem, forKey: Constants.UserDefaults.CURRENT_USER)
        })
    }
    
    static func loadUser(_ id: String, completion: ReturnedUserCompletionBlock) {
        databaseRef.child("\(Constants.Firebase.Path.USER)/\(id)").observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.childrenCount > 0 {
                completion?(User.parseCurrentUser(snapshot.value as! Dictionary<AnyHashable, Any>))
            }
        })
    }
    
    static func loadAlbumCovers(_ userId: String, completion: ReturnedAlbumCoversCompletionBlock) {
        databaseRef.child("\(Constants.Firebase.Path.ALBUM)/\(userId)").observeSingleEvent(of: .value, with: { (snapshot) in
            if let dict = snapshot.value as? Dictionary<AnyHashable, Any> {
                completion?(Album.parseAlbumCovers(dict))
            } else {
                completion?([])
            }
        })
    }
}
