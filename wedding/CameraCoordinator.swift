//
//  CameraCoordinator.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

class CameraCoordinator {
    
    fileprivate var cameraViewController: CameraViewController?
    
    //MARK: - public
    func initCameraView() -> UINavigationController {
        let cameraNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cameraNavigationController") as? UINavigationController
        cameraViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CameraViewController") as? CameraViewController
        cameraNavigationController?.pushViewController(cameraViewController!, animated: false)
        
        return cameraNavigationController!
    }
}
