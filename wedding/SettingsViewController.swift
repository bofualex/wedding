//
//  SettingsViewController.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage
import SVProgressHUD

final class SettingsViewController: UIViewController {

    @IBOutlet weak var userPhoto: UIImageView!
    @IBOutlet weak var backgroundPhoto: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        userPhoto.layer.borderWidth = 4
        userPhoto.layer.borderColor = UIColor.white.cgColor
        userPhoto.image = UIImage(named: "default_user_icon")!.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        userPhoto.tintColor = Constants.UI.selectedColor
        nameLabel.text = FAuth.currentUser?.name
        
        if FAuth.currentUser?.image != nil {
            userPhoto.image = FAuth.currentUser?.image
            backgroundPhoto.image = FAuth.currentUser?.image
        } else if FAuth.currentUser?.photo != nil {
            userPhoto.af_setImage(withURL: URL(string: FAuth.currentUser!.photo!)!, placeholderImage: UIImage(named: "default_user_icon"), completion: { [weak self] (image) in
                FAuth.currentUser!.image = image.result.value
                self?.backgroundPhoto.image = image.result.value
                
                if var dict = UserDefaults.standard.object(forKey: Constants.UserDefaults.CURRENT_USER) as? Dictionary<AnyHashable, Any> {
                    dict[Constants.Firebase.Database.IMAGE] = UIImagePNGRepresentation(image.result.value!) as Data?
                    UserDefaults.standard.set(dict, forKey: Constants.UserDefaults.CURRENT_USER)
                    UserDefaults.standard.synchronize()
                }
            })
        }
    }
    
    //MARK: - action
    @IBAction func onSignOut(_ sender: UIButton) {
        SVProgressHUD.show()
        FAuth.signOut()
    }
}
