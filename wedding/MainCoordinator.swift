//
//  MainCoordinator.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

class MainCoordinator {
    
    fileprivate let homeCoordinator = HomeCoordinator()
    fileprivate let cameraCoordinator = CameraCoordinator()
    fileprivate let settingsCoordinator = SettingsCoordinator()
    
    fileprivate var tabBarController: TabBarViewController?
    
    func initTabBar() -> UITabBarController {
        tabBarController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "TabBarViewController") as? TabBarViewController
        tabBarController?.viewControllers = tabBarControllers()
        
        return tabBarController!
    }
    
    func tabBarControllers() -> [UINavigationController] {
        return [homeCoordinator.initHomeView(), cameraCoordinator.initCameraView(), settingsCoordinator.initSettingsView()]
    }
}
