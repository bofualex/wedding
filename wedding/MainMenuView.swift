//
//  MainMenuView.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit

protocol MainMenuViewDelegate: class {
    func mainMenuViewSelectedCell(_ index: Int)
}

final class MainMenuView: UIView {
    
    @IBOutlet weak var menuTable: UITableView!

    weak var delegate: MainMenuViewDelegate?
    var menuItems = ["Notifications", "View guest list", "Invite guests", "Edit albums", "Edit events"]
    
    //MARK: - public
    class func showMainMenu() -> MainMenuView {
        let mainMenuView: MainMenuView = UIView.fromNib()
        mainMenuView.setupUI()
        
        return mainMenuView
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        menuTable.register(UINib.init(nibName: "MainMenuTableViewCell", bundle: nil), forCellReuseIdentifier: "MainMenuCell")
    }
}

extension MainMenuView: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return menuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let selectCell = tableView.dequeueReusableCell(withIdentifier: "MainMenuCell", for: indexPath as IndexPath) as! MainMenuTableViewCell
        selectCell.setupCell(menuItems[indexPath.row])

        return selectCell
    }
}

extension MainMenuView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.mainMenuViewSelectedCell(indexPath.row)
    }
}
