//
//  HomeCoordinator.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

final class HomeCoordinator {
    
    fileprivate var homeViewController: HomeViewController?
    fileprivate var menuView: MainMenuView?
    
    //MARK: - public
    func initHomeView() -> UINavigationController {
        let homeNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeNavigationController") as? UINavigationController
        homeViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HomeViewController") as? HomeViewController
        homeViewController?.menuView = showMainMenu()
        homeNavigationController?.pushViewController(homeViewController!, animated: false)
        getUserAlbums()

        return homeNavigationController!
    }
    
    //MARK: - private
    fileprivate func showMainMenu() -> MainMenuView {
        menuView = MainMenuView.showMainMenu()
        menuView?.delegate = self
        
        return menuView!
    }
    
    fileprivate func getUserAlbums() {
        FDatabase.loadAlbumCovers(FAuth.currentUser!.uuid, completion: { [weak self] (albums) in
            self?.homeViewController?.albums = albums
            self?.homeViewController?.albumCollectionView.reloadData()
        })
    }
}

extension HomeCoordinator: MainMenuViewDelegate {
    internal func mainMenuViewSelectedCell(_ index: Int) {
        
    }
}
