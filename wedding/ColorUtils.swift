//
//  ColorUtils.swift
//  wedding
//
//  Created by Bofu Alex on 22/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    class func random() -> UIColor {
        let colorArray = [UIColor.white, UIColor.green, UIColor.blue, UIColor.cyan, UIColor.red, UIColor.yellow, UIColor.magenta, UIColor.orange]
        let index = Int(arc4random_uniform(UInt32(colorArray.count)))
        
        return colorArray[index]
    }
}
