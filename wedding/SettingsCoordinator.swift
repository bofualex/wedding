//
//  SettingsCoordinator.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

class SettingsCoordinator {
    
    fileprivate var settingsViewController: SettingsViewController?
    fileprivate var user: User?
    
    //MARK: - public
    func initSettingsView() -> UINavigationController {
        let settingsNavigationController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "settingsNavigationController") as? UINavigationController
        settingsViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController
        settingsNavigationController?.pushViewController(settingsViewController!, animated: false)
        
        return settingsNavigationController!
    }
}
