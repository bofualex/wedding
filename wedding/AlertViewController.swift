//
//  AlertViewController.swift
//  wedding
//
//  Created by Bofu Alex on 19/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit

class AlertViewController: UIViewController {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var confirmButton: UIButton!
    
    fileprivate var titleText: String?
    fileprivate var descriptionText: String?
    fileprivate var buttonText: String?
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - init
    class func alert(_ message: String, title: String, buttonText: String) -> AlertViewController {
        let alert = AlertViewController(nibName: "AlertViewController", bundle: nil)
        alert.titleText = title
        alert.descriptionText = message
        alert.buttonText = buttonText
        
        return alert
    }
    
    class func showAlert(on sender: UIViewController, message: String, title: String, buttonText: String)  {
        let alert = AlertViewController.alert(message, title: title, buttonText: buttonText)
        alert.addModalChild(to: sender)
    }

    //MARK: - action
    @IBAction func onDismiss(_ sender: AnyObject) {
        dismiss(animated: false, completion: nil)
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        titleLabel.text = titleText
        descriptionLabel.text = descriptionText
        confirmButton.setTitle(buttonText, for: .normal)
    }
    
    fileprivate func addModalChild(to sender: UIViewController) {
        providesPresentationContextTransitionStyle = true
        definesPresentationContext = true
        modalPresentationStyle = UIModalPresentationStyle.overFullScreen
        
        if sender.tabBarController != nil {
            sender.tabBarController?.present(self, animated: false, completion: nil)
        } else {
            sender.navigationController?.present(self, animated: false, completion: nil)
        }
    }
}
