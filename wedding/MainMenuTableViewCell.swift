//
//  MainMenuTableViewCell.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit

final class MainMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuName: UILabel!
    @IBOutlet weak var menuIcon: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    //MARK: - public
    func setupCell(_ item: String) {
        menuName.text = item
        menuIcon.setImage(UIImage(named: item), for: .normal)
    }
}
