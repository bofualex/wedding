//
//  AuthentificationViewController.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit
import SVProgressHUD

protocol AuthentificationViewDelegate: class {
    func authentificationViewRequestsSignUp()
    func authentificationViewRequestsSignIn()
}

class AuthentificationViewController: UIViewController {
    
    weak var delegate: AuthentificationViewDelegate?
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        SVProgressHUD.dismiss()
    }
    
    //MARK: - action
    @IBAction func onSignIn(_ sender: UIButton) {
        delegate?.authentificationViewRequestsSignIn()
    }
    
    @IBAction func onSignUp(_ sender: UIButton) {
        delegate?.authentificationViewRequestsSignUp()
    }
}
