//
//  User.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

struct User {
    
    var name: String!
    var email: String!
    var uuid: String!
    var photo: String?
    var image: UIImage?
}

extension User {
    
    static func parseUserFromAuth(_ user: FIRUser, name: String) -> User {
        var userToReturn = User()
        
        userToReturn.name = name
        userToReturn.email = user.email
        userToReturn.uuid = user.uid
        
        return userToReturn
    }
    
    static func parseCurrentUser(_ dict: Dictionary<AnyHashable, Any>) -> User {
        var currentUser = User()
        
        if let name = dict[Constants.Firebase.Database.NAME] {
            currentUser.name = name as! String
        }
        
        if let email = dict[Constants.Firebase.Database.EMAIL] {
            currentUser.email = email as! String
        }
        
        if let uuid = dict[Constants.Firebase.Database.UUID] {
            currentUser.uuid = uuid as! String
        }
        
        if let photo = dict[Constants.Firebase.Database.PHOTO] {
            currentUser.photo = photo as? String
        }
        
        if let image = dict[Constants.Firebase.Database.IMAGE] {
            currentUser.image = UIImage(data: (image as! Data))
        }
        
        return currentUser
    }
}
