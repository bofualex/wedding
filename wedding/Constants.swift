//
//  Constants.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

typealias ReturnedUserCompletionBlock = ((User) -> ())?
typealias ReturnedAlbumCoversCompletionBlock = (([Album]) -> ())?

struct Constants {
    struct UI {
        static let selectedColor = UIColor(red:0.55, green:0.77, blue:0.25, alpha:1.0)
        static let unselectedColor = UIColor.white
        static let backgroundColor = UIColor(red:0.55, green:0.77, blue:0.25, alpha:1.0)
    }
    
    struct Firebase {
        
        struct Path {
            static let USER               = "USER"
            static let ALBUM              = "ALBUM"
        }
        
        struct Database {
            static let USER               = "user"
            static let NAME               = "name"
            static let UUID               = "uuid"
            static let EMAIL              = "email"
            static let PHOTO              = "photo"
            static let CREATEDAT          = "createdAt"
            static let MODIFIEDAT         = "modifiedAt"
            static let IMAGE              = "image"
            static let COVER              = "cover"
        }
    }
    
    struct Notification {
        static let USER_SIGNED_IN           = "USER_SIGNED_IN"
        static let USER_SIGNED_OUT          = "USER_SIGNED_OUT"
    }
    
    struct UserDefaults {
        static let CURRENT_USER             = "CURRENT_USER"
    }
}
