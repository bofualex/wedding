//
//  HomeViewController.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit
import FLKAutoLayout

final class HomeViewController: UIViewController {

    @IBOutlet weak var mainMenuContainer: UIView!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var mainViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var mainViewTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var albumCollectionView: UICollectionView!

    var albums = [Album]()
    var menuView: MainMenuView?
    
    fileprivate let sectionInsets = UIEdgeInsets(top: 10.0, left: 10.0, bottom: 10.0, right: 10.0)
    fileprivate let itemsPerRow = 2
    fileprivate let heightConstant: CGFloat = 1.64
    fileprivate var mainViewLeadingConstant: CGFloat {
        if (2 * view.frame.width / 3) < 300 {
            return 2 * view.frame.width / 3
        } else {
            return 2 * view.frame.height / 3
        }
    }
    
    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        mainMenuContainer.addSubview(menuView!)
        menuView?.align(toView: mainMenuContainer)
    }
    
    //MARK: - action
    @IBAction func onMainMenu() {
        if mainViewLeadingConstraint.constant == 0 {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.mainViewLeadingConstraint.constant = self!.mainViewLeadingConstant
                self?.mainViewTrailingConstraint.constant -= self!.mainViewLeadingConstant
                self?.view.layoutIfNeeded()
            })
        } else {
            UIView.animate(withDuration: 0.3, animations: { [weak self] in
                self?.mainViewLeadingConstraint.constant = 0
                self?.mainViewTrailingConstraint.constant = 0
                self?.view.layoutIfNeeded()
            })
        }
    }
}

extension HomeViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return albums.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AlbumCell", for: indexPath) as! AlbumCollectionViewCell
        cell.setupCell(albums[indexPath.row])
        
        if albums[indexPath.row].albumCoverImage != nil {
            cell.albumImageView.image = albums[indexPath.row].albumCoverImage!
        } else {
            cell.albumImageView.af_setImage(withURL: URL(string: albums[indexPath.row].albumCoverLink!)!, placeholderImage: UIImage(named: "lightGreen"), completion: { [weak self] (imageResponse) in
                self?.albums[indexPath.row].albumCoverImage = imageResponse.result.value
                self?.albumCollectionView.reloadItems(at: [indexPath])
            })
        }
        
        return cell
    }
}

extension HomeViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let paddingSpace = sectionInsets.left * (CGFloat(itemsPerRow) + 1)
        let availableWidth = view.frame.width - paddingSpace
        let widthPerItem = availableWidth / CGFloat(itemsPerRow)
        
        if let image = albums[indexPath.row].albumCoverImage {
            if image.size.width > image.size.height {
                return CGSize(width: widthPerItem, height: widthPerItem / heightConstant)
            } else {
                return CGSize(width: widthPerItem / heightConstant, height: widthPerItem)
            }
        } else {
            return CGSize(width: widthPerItem, height: widthPerItem / heightConstant)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return sectionInsets
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return sectionInsets.left
    }
}

