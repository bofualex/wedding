//
//  SignUpViewController.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol SignUpViewDelegate: class {
    func signUpViewRequestsSignUp(_ name: String, email: String, password: String, photo: Data?)
}

class SignUpViewController: UIViewController {

    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordField: UITextField!
    @IBOutlet weak var userPhotoButton: UIButton!
    @IBOutlet weak var innerView: UIScrollView!
    
    weak var delegate: SignUpViewDelegate?
    
    fileprivate var activeField: UITextField?
    fileprivate var photoData: Data?

    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addNotifications()
    }

    //MARK: - memory management
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - private
    fileprivate func addNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    //MARK: - action
    @IBAction func onBack(_ sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pressedAttachPhoto(sender: UIButton) {
        view.endEditing(true)
        
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = false
        
        let cameraAlert = UIAlertController(title: title, message: "Take a photo of your dish", preferredStyle: UIAlertControllerStyle.alert)
        cameraAlert.addAction(UIAlertAction(title: "Take Photo", style: .default, handler: { [weak self]
            (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.camera
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        cameraAlert.addAction(UIAlertAction(title: "Choose Photo", style: .default, handler: { [weak self]
            (action: UIAlertAction) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.photoLibrary) {
                imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
                self?.present(imagePicker, animated: true, completion: nil)
            }
        }))
        
        cameraAlert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (action: UIAlertAction!) in
        }))
        
        present(cameraAlert, animated: true, completion: nil)
    }

    @IBAction func onSignUp(_ sender: UIButton) {
        guard emailTextField.text!.characters.count > 3 && emailTextField.text!.contains("@") else {
            AlertViewController.showAlert(on: self, message: "Email address is badly formatted", title: "Email address", buttonText: "OK")
            return
        }
        
        guard nameTextField.text!.characters.count > 3 else {
            AlertViewController.showAlert(on: self, message: "Please enter a longer name", title: "Longer name required", buttonText: "OK")
            return
        }
        
        guard passwordTextField.text!.characters.count > 4 else {
            AlertViewController.showAlert(on: self, message: "Please enter a longer password", title: "Longer password required", buttonText: "OK")
            return
        }
        
        guard passwordTextField.text! == confirmPasswordField.text! else {
            AlertViewController.showAlert(on: self, message: "Passwords don't match", title: "Re-enter passwords", buttonText: "OK")
            return
        }
        
        SVProgressHUD.show()
        delegate?.signUpViewRequestsSignUp(nameTextField.text!, email: emailTextField.text!, password: passwordTextField.text!, photo: photoData)
    }
}

extension SignUpViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
        dismiss(animated: true, completion: nil)
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage {
            userPhotoButton.setImage(pickedImage, for: .normal)
            photoData = UIImageJPEGRepresentation(pickedImage, 0.3)!
        }
    }
}

extension SignUpViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeField = nil
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeField = textField
    }
}

extension SignUpViewController {
    func keyboardWillShow(notification: NSNotification) {
        if let activeField = activeField, let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            let contentInsets = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardSize.height, right: 0.0)
            innerView.contentInset = contentInsets
            innerView.scrollIndicatorInsets = contentInsets
            var aRect = self.view.frame
            aRect.size.height -= keyboardSize.size.height
            if (!aRect.contains(activeField.frame.origin)) {
                innerView.scrollRectToVisible(activeField.frame, animated: true)
            }
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        let contentInsets = UIEdgeInsets.zero
        innerView.contentInset = contentInsets
        innerView.scrollIndicatorInsets = contentInsets
    }
}
