//
//  AuthCoordinator.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import UIKit

final class AuthCoordinator {
    
    fileprivate var authViewController: AuthentificationViewController?
    fileprivate var signUpViewController: SignUpViewController?
    fileprivate var signInViewController: SignInViewController?
    
    //MARK: - public
    func showAuthView() -> UINavigationController {
        let authNavController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "authNavigationController") as? UINavigationController
        authViewController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "AuthentificationViewController") as? AuthentificationViewController
        authViewController?.delegate = self
        authNavController?.pushViewController(authViewController!, animated: false)
        
        return authNavController!
    }
    
    //MARK: - private
    fileprivate func showSignUpView() {
        signUpViewController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController") as? SignUpViewController
        signUpViewController?.delegate = self
        
        authViewController!.navigationController?.pushViewController(signUpViewController!, animated: true)
    }
    
    fileprivate func showSignInView() {
        signInViewController = UIStoryboard(name: "Auth", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController") as? SignInViewController
        signInViewController?.delegate = self
        
        authViewController!.navigationController?.pushViewController(signInViewController!, animated: true)
    }
    
    fileprivate func signUpUser(_ name: String, email: String, password: String, photo: Data?) {
        FAuth.createUser(name: name, email: email, password: password, photo: photo, completion: { [weak self] (error) in
            if error != "" {
                AlertViewController.showAlert(on: self!.signUpViewController!, message: error, title: "Something went wrong", buttonText: "OK")
            }
        })
    }
    
    fileprivate func signInUser(_ email: String, password: String) {
        FAuth.login(email: email, password: password, completion: nil)
    }
}

extension AuthCoordinator: AuthentificationViewDelegate {
    func authentificationViewRequestsSignIn() {
        showSignInView()
    }

    func authentificationViewRequestsSignUp() {
        showSignUpView()
    }
}

extension AuthCoordinator: SignUpViewDelegate {
    func signUpViewRequestsSignUp(_ name: String, email: String, password: String, photo: Data?) {
        signUpUser(name, email: email, password: password, photo: photo)
    }    
}

extension AuthCoordinator: SignInViewDelegate {
    func signInViewRequestsSignIn(email: String, password: String) {
        signInUser(email, password: password)
    }
}
