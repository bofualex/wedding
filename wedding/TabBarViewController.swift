//
//  TabBarViewController.swift
//  wedding
//
//  Created by Bofu Alex on 17/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import UIKit

final class TabBarViewController: UITabBarController {

    //MARK: - view life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    //MARK: - private
    fileprivate func setupUI() {
        tabBar.tintColor = Constants.UI.selectedColor
        tabBar.backgroundColor = Constants.UI.backgroundColor

        if #available(iOS 10.0, *) {
            tabBar.unselectedItemTintColor = Constants.UI.unselectedColor
        } else {
            
        }
    }
}
