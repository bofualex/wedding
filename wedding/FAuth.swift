//
//  FAuth.swift
//  wedding
//
//  Created by Bofu Alex on 18/02/2017.
//  Copyright © 2017 Bofu Alex. All rights reserved.
//

import Foundation
import FirebaseAuth
import SVProgressHUD

struct FAuth {
    
    static var currentUser: User?
    
    static func loadCurrentUser() {
        if UserDefaults.standard.object(forKey: Constants.UserDefaults.CURRENT_USER) != nil {
            currentUser = User.parseCurrentUser(UserDefaults.standard.object(forKey: Constants.UserDefaults.CURRENT_USER) as! Dictionary<AnyHashable, Any>)
        } else {
            SVProgressHUD.show()
            FDatabase.loadUser(FIRAuth.auth()!.currentUser!.uid, completion: { (user) in
                SVProgressHUD.dismiss()
                currentUser = user
                var dict = [Constants.Firebase.Database.NAME: user.name, Constants.Firebase.Database.EMAIL: user.email, Constants.Firebase.Database.UUID: user.uuid]
                
                if user.photo != nil {
                    dict[Constants.Firebase.Database.PHOTO] = user.photo
                }
                
                saveLocally(dict, forKey: Constants.UserDefaults.CURRENT_USER)
            })
        }
    }
    
    static func loadUser(_ id: String = FIRAuth.auth()!.currentUser!.uid, completion: ReturnedUserCompletionBlock) {
        FDatabase.loadUser(id, completion: { (user) in
            completion?(user)
        })
    }
    
    static func login(email: String, password: String, completion: ((String) -> ())?) {
        FIRAuth.auth()?.signIn(withEmail: email, password: password) { (user, error) in
            if (error != nil) {
                completion?(error!.localizedDescription)
            } else {
                completion?("")
                NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.USER_SIGNED_IN), object: nil)
            }
        }
    }
    
    static func createUser(name: String, email: String, password: String, photo: Data?, completion: ((String) -> ())?) {
        FIRAuth.auth()?.createUser(withEmail: email, password: password) { (user, error) in
            if error == nil {
                login(email: email, password: password, completion: nil)
                completion?("")
                let currentUser = User.parseUserFromAuth(user!, name: name)
                
                if photo != nil {
                    FStorage.uploadUserPhoto(photo!, user: currentUser)
                } else {
                    FDatabase.createUser(currentUser)
                }
            } else {
                completion?(error!.localizedDescription)
            }
        }
    }
    
    static func signOut() {
        SVProgressHUD.show()
        try! FIRAuth.auth()!.signOut()
        saveLocally(nil, forKey: Constants.UserDefaults.CURRENT_USER)
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.Notification.USER_SIGNED_OUT), object: nil)
    }
    
    static func saveLocally(_ dict: Dictionary<AnyHashable, Any>?, forKey key: String) {
        if dict == nil {
            UserDefaults.standard.removeObject(forKey: key)
        } else {
            UserDefaults.standard.set(dict, forKey: key)
        }
        
        UserDefaults.standard.synchronize()
    }
}
